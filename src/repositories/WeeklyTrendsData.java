package repositories;

import models.CommonFields;
import models.WeeklyTrends;
import workbook.PrepareExcel;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.sql.Connection;

/**
 * Created by Phani.Teja on 14-Feb-17.
 */
public class WeeklyTrendsData extends Base implements RetrieveData{

    public ArrayList<CommonFields> getDataFromDB(){
        Connection connection= Base.getConnection();
        Statement stmt;
        ResultSet rs=null;
        try {
            stmt = connection.createStatement();
            String endDate=PrepareExcel.endDate;
            String accountId=PrepareExcel.accountId;
            String query="CALL CalculateWeeklyData('2017-01-24' ,'"+ endDate+"' , '"+accountId+"')";
            System.out.println(query); 
            System.out.println(query);
            System.out.println(query);
            
            rs=stmt.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return populateResultSet(rs);
    }


    public ArrayList<CommonFields> populateResultSet(ResultSet rs) {
        if(rs==null){
            return null;
        }
        ArrayList<CommonFields> finalData=new ArrayList<>();
        try {
            while (rs.next()) {
                String end_of_week = rs.getString("end_of_week");
                int impressions = rs.getInt("impressions");
                int clicks = rs.getInt("clicks");
                float ctr = rs.getFloat("CTR");
                int cost = rs.getInt("cost");
                float cpc = rs.getFloat("CPC");
                int conversions = rs.getInt("conversions");
                float convRate = rs.getFloat("convRate");
                float cpa = rs.getFloat("CPA");
                int revenue = rs.getInt("revenue");
                float roas = rs.getFloat("ROAS");
                float avgPos = rs.getFloat("avgPos");

                WeeklyTrends row = new WeeklyTrends(end_of_week, impressions, clicks, ctr, cost, cpc, conversions, convRate, cpa, revenue, roas, avgPos);

                finalData.add(row);
            }
            return finalData;
        } catch(SQLException e){
            System.out.println(e.getStackTrace());
        }

        return null;
    }

}
