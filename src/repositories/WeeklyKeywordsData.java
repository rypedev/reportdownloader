package repositories;

import models.*;
import workbook.PrepareExcel;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.sql.Connection;

/**
 * Created by Phani.Teja on 14-Feb-17.
 */
public class WeeklyKeywordsData extends Base implements RetrieveData{
    public String query=null;

    public ArrayList<CommonFields> getDataFromDB(){
        Connection connection= Base.getConnection();
        Statement stmt;
        ResultSet rs=null;
        try {
            stmt = connection.createStatement();
            System.out.println(query); 
            rs=stmt.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return populateResultSet(rs);
    }

    public ArrayList<CommonFields> getCurrentWeeksData(){
        String endDate= PrepareExcel.endDate;
        String accountId=PrepareExcel.accountId;
        String query= "SELECT  campaign_name AS campaignName, ad_group_name AS adGroupName , criteria AS keywords, SUM(impressions) AS impressions , SUM(clicks) AS clicks,COALESCE((SUM(clicks)/SUM(impressions)),0)*100 AS CTR, SUM(cost) AS cost, COALESCE(SUM(cost)/SUM(clicks),0) AS CPC, SUM(conversions) AS conversions,COALESCE((SUM(conversions)/SUM(clicks)),0)*100 AS convRate, COALESCE(SUM(cost)/SUM(conversions),0) AS CPA, SUM(COALESCE(revenue,0)) AS revenue, COALESCE(((SUM(COALESCE(revenue,0))-SUM(cost))/SUM(cost)),0)*100 AS ROAS, COALESCE(SUM(impressions*average_position)/SUM(Impressions),0) AS avgPos FROM rypereportkeywords WHERE account_id =  '"+accountId+"' AND DATEDIFF('"+endDate+"',date_of_report) <= 6 GROUP BY keyword_id;";
        this.query=query;
        return getDataFromDB();
    }

    public ArrayList<CommonFields> getLastWeeksData(){
        String endDate=PrepareExcel.endDate;
        String accountId=PrepareExcel.accountId;
        String query= "SELECT  campaign_name AS campaignName, ad_group_name AS adGroupName , criteria AS keywords, SUM(impressions) AS impressions , SUM(clicks) AS clicks,COALESCE((SUM(clicks)/SUM(impressions)),0)*100 AS CTR, SUM(cost) AS cost, COALESCE(SUM(cost)/SUM(clicks),0) AS CPC, SUM(conversions) AS conversions,COALESCE((SUM(conversions)/SUM(clicks)),0)*100 AS convRate, COALESCE(SUM(cost)/SUM(conversions),0) AS CPA, SUM(COALESCE(revenue,0)) AS revenue, COALESCE(((SUM(COALESCE(revenue,0))-SUM(cost))/SUM(cost)),0)*100 AS ROAS, COALESCE(SUM(impressions*average_position)/SUM(Impressions),0) AS avgPos FROM rypereportkeywords WHERE account_id =  '"+accountId+"' AND DATEDIFF('"+endDate+"',date_of_report)  between 7 and 14 GROUP BY keyword_id;";
        this.query=query;
        return getDataFromDB();
    }




    public ArrayList<CommonFields> populateResultSet(ResultSet rs) {
        if(rs==null){
            return null;
        }
        ArrayList<CommonFields> finalData=new ArrayList<>();
        try {
            while (rs.next()) {
                String adGroupName = rs.getString("adGroupName");
                String campaignName = rs.getString("campaignName");
                String keyword=rs.getString("keywords");
                int impressions = rs.getInt("impressions");
                int clicks = rs.getInt("clicks");
                float ctr = rs.getFloat("CTR");
                int cost = rs.getInt("cost");
                float cpc = rs.getFloat("CPC");
                int conversions = rs.getInt("conversions");
                float convRate = rs.getFloat("convRate");
                float cpa = rs.getFloat("CPA");
                int revenue = rs.getInt("revenue");
                float roas = rs.getFloat("ROAS");
                float avgPos = rs.getFloat("avgPos");

                WeeklyKeywords row = new WeeklyKeywords(campaignName,adGroupName,keyword, impressions, clicks, ctr, cost, cpc, conversions, convRate, cpa, revenue, roas, avgPos);

                finalData.add(row);
            }
            return finalData;
        } catch(SQLException e){
            System.out.println(e.getStackTrace());
        }

        return null;
    }

}
