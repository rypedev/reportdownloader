package repositories;

import models.CommonFields;
import models.WeeklyAdGroup;
import models.WeeklyCampaign;
import models.WeeklyTrends;
import workbook.PrepareExcel;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.sql.Connection;

/**
 * Created by Phani.Teja on 14-Feb-17.
 */
public class WeeklyAdGroupData extends Base implements RetrieveData{
    public String query=null;

    public ArrayList<CommonFields> getDataFromDB(){
        Connection connection= Base.getConnection();
        Statement stmt;
        ResultSet rs=null;
        try {
            stmt = connection.createStatement();
            System.out.println(query); 
            rs=stmt.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return populateResultSet(rs);
    }

    public ArrayList<CommonFields> getCurrentWeeksData(){
        String endDate= PrepareExcel.endDate;
        String accountId=PrepareExcel.accountId;
        String query= "SELECT  c.campaign_name AS campaignName, a.adgroup_name AS adGroupName, SUM(a.impressions) AS impressions, SUM(a.clicks) AS clicks, COALESCE((SUM(a.clicks)/SUM(a.impressions)),0)*100 AS CTR,SUM(a.cost) AS cost, COALESCE(SUM(a.cost)/SUM(a.clicks),0) AS CPC, SUM(a.conversions) AS conversions, COALESCE((SUM(a.conversions)/SUM(a.clicks)),0)*100 AS convRate,COALESCE(SUM(a.cost)/SUM(a.conversions),0) AS CPA, SUM(COALESCE(a.revenue,0)) AS revenue, COALESCE(((SUM(COALESCE(a.revenue,0))-SUM(a.cost))/SUM(a.cost)),0)*100 AS ROAS, COALESCE(SUM(a.impressions*a.average_position)/SUM(a.Impressions),0) AS avgPos FROM rypereportadgroup a,rypereportcampaign c WHERE a.campaign_id = c.campaign_id AND a.account_id =  '"+accountId+"' AND DATEDIFF('"+endDate+"',a.date_of_report) <= 6 GROUP BY a.adgroup_id;";
        this.query=query;
        return getDataFromDB();
    }

    public ArrayList<CommonFields> getLastWeeksData(){
        String endDate=PrepareExcel.endDate;
        String accountId=PrepareExcel.accountId;
        String query= "SELECT  c.campaign_name AS campaignName, a.adgroup_name AS adGroupName, SUM(a.impressions) AS impressions, SUM(a.clicks) AS clicks, COALESCE((SUM(a.clicks)/SUM(a.impressions)),0)*100 AS CTR,SUM(a.cost) AS cost, COALESCE(SUM(a.cost)/SUM(a.clicks),0) AS CPC, SUM(a.conversions) AS conversions, COALESCE((SUM(a.conversions)/SUM(a.clicks)),0)*100 AS convRate,COALESCE(SUM(a.cost)/SUM(a.conversions),0) AS CPA, SUM(COALESCE(a.revenue,0)) AS revenue, COALESCE(((SUM(COALESCE(a.revenue,0))-SUM(a.cost))/SUM(a.cost)),0)*100 AS ROAS, COALESCE(SUM(a.impressions*a.average_position)/SUM(a.Impressions),0) AS avgPos FROM rypereportadgroup a,rypereportcampaign c WHERE a.campaign_id = c.campaign_id AND a.account_id =  '"+accountId+"' AND DATEDIFF('"+endDate+"',a.date_of_report) between 7 and 14 GROUP BY a.adgroup_id;";
        this.query=query;
        return getDataFromDB();
    }




    public ArrayList<CommonFields> populateResultSet(ResultSet rs) {
        if(rs==null){
            return null;
        }
        ArrayList<CommonFields> finalData=new ArrayList<>();
        try {
            while (rs.next()) {
                String adGroupName = rs.getString("adGroupName");
                String campaignName = rs.getString("campaignName");
                int impressions = rs.getInt("impressions");
                int clicks = rs.getInt("clicks");
                float ctr = rs.getFloat("CTR");
                int cost = rs.getInt("cost");
                float cpc = rs.getFloat("CPC");
                int conversions = rs.getInt("conversions");
                float convRate = rs.getFloat("convRate");
                float cpa = rs.getFloat("CPA");
                int revenue = rs.getInt("revenue");
                float roas = rs.getFloat("ROAS");
                float avgPos = rs.getFloat("avgPos");

                WeeklyAdGroup row = new WeeklyAdGroup(campaignName,adGroupName, impressions, clicks, ctr, cost, cpc, conversions, convRate, cpa, revenue, roas, avgPos);

                finalData.add(row);
            }
            return finalData;
        } catch(SQLException e){
            System.out.println(e.getStackTrace());
        }

        return null;
    }

}
