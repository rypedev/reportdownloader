package repositories;

import models.CommonFields;
import models.WeeklyTrends;

import java.sql.ResultSet;
import java.util.ArrayList;

/**
 * Created by Phani.Teja on 14-Feb-17.
 */
public interface RetrieveData {

    public ArrayList<CommonFields> getDataFromDB();
    public ArrayList<CommonFields> populateResultSet(ResultSet rs);

}
