package repositories;

import models.CommonFields;
import models.MonthlyTrends;
import workbook.PrepareExcel;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.sql.Connection;

/**
 * Created by Phani.Teja on 14-Feb-17.
 */
public class MonthlyTrendsData extends Base implements RetrieveData{

    public ArrayList<CommonFields> getDataFromDB(){
        Connection connection= Base.getConnection();
        Statement stmt;
        ResultSet rs=null;
        ArrayList<CommonFields> trends=null;

        PrepareExcel prepareExcel=new PrepareExcel();
        String endDate= prepareExcel.getEndDate();
        String accountId=prepareExcel.getAccountId();
        try {
            stmt = connection.createStatement();
          String query="SELECT SUBSTR(DATE_OF_REPORT,1,4) AS YEAR,SUBSTR(DATE_OF_REPORT,6,2) AS MONTH, (DATE_FORMAT(date_of_report,'%b,%Y')) AS monthinfo,SUM(impressions) AS impressions , SUM(clicks) AS clicks, COALESCE((SUM(clicks)/SUM(impressions)),0)*100 AS CTR,SUM(cost) AS cost, COALESCE(SUM(cost)/SUM(clicks),0) AS CPC, SUM(conversions) AS conversions, COALESCE((SUM(conversions)/SUM(clicks)),0)*100 AS convRate,COALESCE(SUM(cost)/SUM(conversions),0) AS CPA, SUM(COALESCE(revenue,0)) AS revenue, COALESCE(((SUM(COALESCE(revenue,0))-SUM(cost))/SUM(cost)),0)*100 AS ROAS,COALESCE(SUM(impressions*average_position)/SUM(Impressions),0) AS avgPos FROM rypereportcampaign WHERE account_id = '"+accountId+"' GROUP BY SUBSTR(date_of_report,1,7)";
          System.out.println(query); 
          rs=stmt.executeQuery(query);
            //System.out.println(rs.getMetaData());
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            trends= populateResultSet(rs);
            try{
                connection.close();
            } catch(SQLException e){
                System.out.println("not able to close connection");
            }
        }
        return trends;
    }


    public ArrayList<CommonFields> populateResultSet(ResultSet rs) {
        if(rs==null){
            return null;
        }
        ArrayList<CommonFields> finalData=new ArrayList<>();
        try {
            while (rs.next()) {
                String month = rs.getString("monthinfo");
                int impressions = rs.getInt("impressions");
                int clicks = rs.getInt("clicks");
                float ctr = rs.getFloat("CTR");
                int cost = rs.getInt("cost");
                float cpc = rs.getFloat("CPC");
                int conversions = rs.getInt("conversions");
                float convRate = rs.getFloat("convRate");
                float cpa = rs.getFloat("CPA");
                int revenue = rs.getInt("revenue");
                float roas = rs.getFloat("ROAS");
                float avgPos = rs.getFloat("avgPos");

                MonthlyTrends row = new MonthlyTrends(month, impressions, clicks, ctr, cost, cpc, conversions, convRate, cpa, revenue, roas, avgPos);

                finalData.add(row);
            }
            return finalData;
        } catch(SQLException e){
            System.out.println(e.getStackTrace());
        }

        return null;
    }

}
