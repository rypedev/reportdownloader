package repositories;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Phani.Teja on 14-Feb-17.
 */
public class Base {

    public static Connection connection;
    public static String connectionUrl="jdbc:mysql://localhost:3306/semreport";
    public static String username="root";
    public static String password="root";

    public static  Connection getConnection(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection= DriverManager.getConnection(connectionUrl,username,password);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return connection;
    }
}
