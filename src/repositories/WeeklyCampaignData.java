package repositories;

import models.CommonFields;
import models.WeeklyCampaign;
import models.WeeklyTrends;
import workbook.PrepareExcel;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.sql.Connection;

/**
 * Created by Phani.Teja on 14-Feb-17.
 */
public class WeeklyCampaignData extends Base implements RetrieveData{
    public String query=null;

    public ArrayList<CommonFields> getDataFromDB(){
        Connection connection= Base.getConnection();
        Statement stmt;
        ResultSet rs=null;
        try {
            stmt = connection.createStatement();
            System.out.println(query); 
            rs=stmt.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return populateResultSet(rs);
    }

    public ArrayList<CommonFields> getCurrentWeeksData(){
        String endDate= PrepareExcel.endDate;
        String accountId=PrepareExcel.accountId;
        String query= "SELECT  campaign_name AS campaignName, SUM(impressions) AS impressions , SUM(clicks) AS clicks, COALESCE((SUM(clicks)/SUM(impressions)),0)*100 AS CTR,SUM(cost) AS cost, COALESCE(SUM(cost)/SUM(clicks),0) AS CPC, SUM(conversions) AS conversions, COALESCE((SUM(conversions)/SUM(clicks)),0)*100 AS convRate,COALESCE(SUM(cost)/SUM(conversions),0) AS CPA, SUM(COALESCE(revenue,0)) AS revenue, COALESCE(((SUM(COALESCE(revenue,0))-SUM(cost))/SUM(cost)),0)*100 AS ROAS, COALESCE(SUM(impressions*average_position)/SUM(Impressions),0) AS avgPos FROM rypereportcampaign WHERE account_id =  '"+accountId+"' AND DATEDIFF('"+endDate+"',date_of_report) <= 6 GROUP BY campaign_id;";
        this.query=query;
        return getDataFromDB();
    }

    public ArrayList<CommonFields> getLastWeeksData(){
        String endDate=PrepareExcel.endDate;
        String accountId=PrepareExcel.accountId;
        String query= "SELECT  campaign_name AS campaignName, SUM(impressions) AS impressions , SUM(clicks) AS clicks, COALESCE((SUM(clicks)/SUM(impressions)),0)*100 AS CTR,SUM(cost) AS cost, COALESCE(SUM(cost)/SUM(clicks),0) AS CPC, SUM(conversions) AS conversions, COALESCE((SUM(conversions)/SUM(clicks)),0)*100 AS convRate,COALESCE(SUM(cost)/SUM(conversions),0) AS CPA, SUM(COALESCE(revenue,0)) AS revenue, COALESCE(((SUM(COALESCE(revenue,0))-SUM(cost))/SUM(cost)),0)*100 AS ROAS, COALESCE(SUM(impressions*average_position)/SUM(Impressions),0) AS avgPos FROM rypereportcampaign WHERE account_id =  '"+accountId+"' AND DATEDIFF('"+endDate+"',date_of_report) between 7 and 14 GROUP BY campaign_id;";
        this.query=query;
        return getDataFromDB();
    }




    public ArrayList<CommonFields> populateResultSet(ResultSet rs) {
        if(rs==null){
            return null;
        }
        ArrayList<CommonFields> finalData=new ArrayList<>();
        try {
            while (rs.next()) {
                String campaignName = rs.getString("campaignName");
                int impressions = rs.getInt("impressions");
                int clicks = rs.getInt("clicks");
                float ctr = rs.getFloat("CTR");
                int cost = rs.getInt("cost");
                float cpc = rs.getFloat("CPC");
                int conversions = rs.getInt("conversions");
                float convRate = rs.getFloat("convRate");
                float cpa = rs.getFloat("CPA");
                int revenue = rs.getInt("revenue");
                float roas = rs.getFloat("ROAS");
                float avgPos = rs.getFloat("avgPos");

                WeeklyCampaign row = new WeeklyCampaign(campaignName, impressions, clicks, ctr, cost, cpc, conversions, convRate, cpa, revenue, roas, avgPos);

                finalData.add(row);
            }
            return finalData;
        } catch(SQLException e){
            System.out.println(e.getStackTrace());
        }

        return null;
    }

}
