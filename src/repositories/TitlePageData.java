package repositories;

import models.TitlePage;
import workbook.PrepareExcel;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Created by Phani.Teja on 16-Feb-17.
 */
public class TitlePageData extends Base{
    public TitlePage getDataFromDB(){
        Connection connection= Base.getConnection();
        Statement stmt;
        ResultSet rs=null;
        try {
            stmt = connection.createStatement();
            String endDate= PrepareExcel.endDate;
            String accountId=PrepareExcel.accountId;
            accountId=accountId.substring(0,3)+"-"+accountId.substring(3,6)+"-"+accountId.substring(6);
            String query="SELECT networkAccountId AS accountId, NAME AS accountName, address AS address,company AS companyName,weburl AS webUrl, managername AS managerName FROM `account` WHERE networkAccountId='"+accountId+"';";
            System.out.println(query); 
            rs=stmt.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return populateResultSet(rs);
    }

    public TitlePage populateResultSet(ResultSet rs) {
        TitlePage titlePage=null;

        try {
            while(rs.next()) {
                String accountName = rs.getString("accountName");
                String address = rs.getString("address");
                String companyName = rs.getString("companyName");
                String webUrl = rs.getString("webUrl");
                String managerName = rs.getString("managerName");

                titlePage = new TitlePage(accountName, address, companyName, webUrl, managerName);
            }
        } catch (SQLException e){
            System.out.println(e.getStackTrace());
        }
        return titlePage;
    }

}
