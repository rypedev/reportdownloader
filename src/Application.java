import workbook.PrepareExcel;

/**
 * Created by Phani.Teja on 14-Feb-17.
 */
public class Application {
    public static String accountId;
    public static String endDate;
    public static String fileName;

    public static void main(String[] args) {

        if(args.length>0){
            accountId=args[0].replace("-","");
            endDate=args[1];
            fileName= args[2];
        }
        PrepareExcel excel=new PrepareExcel(accountId,endDate,fileName);
        excel.getWorkbook();
    }
}
