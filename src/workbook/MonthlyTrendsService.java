package workbook;

import models.CommonFields;
import models.MonthlyTrends;
import org.apache.poi.ss.format.CellFormatType;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Chart;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.charts.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.util.SystemOutLogger;
import org.apache.poi.xssf.usermodel.*;
import org.apache.poi.xssf.usermodel.charts.XSSFChartLegend;
import org.apache.poi.xssf.usermodel.charts.XSSFLineChartData;
import repositories.MonthlyTrendsData;
import java.util.ArrayList;

public class MonthlyTrendsService extends  PopulateDataService{
    public static XSSFSheet sheet;

    public void removeRows(int start,int end) {
        System.out.println("removing rows from "+start+" to "+end);
        XSSFRow row;
        for(int i=start;i<=end;i++){
            //sheet.shiftRows(i,i,start-i-1);
		/*	System.out.println(i);
			row=sheet.getRow(i);
			sheet.removeRow(row);*/
        }
    }

    public void prepareSheet(XSSFSheet sheet){
        this.sheet=sheet;
        MonthlyTrendsData data=new MonthlyTrendsData();
        ArrayList<CommonFields> list=data.getDataFromDB();

        int firstRowNumber=27;
        int currentRowNumber=firstRowNumber;
        XSSFCellStyle style= sheet.getRow(27).getRowStyle();

        XSSFRow currentRow;
        int totalRows=100;
        int dataRows=list.size();
        int lastRowNumber=sheet.getLastRowNum();
        sheet.shiftRows(lastRowNumber,lastRowNumber,dataRows);
		/*if(dataRows<totalRows){
			removeRows(firstRowNumber+dataRows,firstRowNumber+totalRows);
		}*/

        for(CommonFields dataRow:list){
            MonthlyTrends row=(MonthlyTrends) dataRow;
            currentRow=sheet.createRow(currentRowNumber);
            currentRow.setRowStyle(style);
            currentRow.createCell(1).setCellValue(row.getMonth());
            currentRow.createCell(2).setCellValue(row.getImpressions());
            currentRow.createCell(3).setCellValue(row.getClicks());
            currentRow.createCell(4).setCellValue(row.getCtr());
            currentRow.createCell(5).setCellValue(row.getCost());
            currentRow.createCell(6).setCellValue(row.getCpc());
            currentRow.createCell(7).setCellValue(row.getConversions());
            currentRow.createCell(8).setCellValue(row.getConvRate());
            currentRow.createCell(9).setCellValue(row.getCpa());
            currentRow.createCell(10).setCellValue(row.getRevenue());
            currentRow.createCell(11).setCellValue(row.getRoas());
            currentRow.createCell(12).setCellValue(row.getAvgPos());
            currentRowNumber++;
        }


        lastRowNumber=firstRowNumber+dataRows;
        System.out.println("last row :"+lastRowNumber);
        XSSFRow lastRow= sheet.getRow(sheet.getLastRowNum());

        CommonFields row=calculateFinalValues(list);
        //row.print();
        lastRow.getCell(2).setCellValue(row.getImpressions());
        lastRow.getCell(3).setCellValue(row.getClicks());
        lastRow.getCell(5).setCellValue(row.getCost());
        lastRow.getCell(7).setCellValue(row.getConversions());
        lastRow.getCell(10).setCellValue(row.getRevenue());

        lastRow.getCell(4).setCellValue(row.getCtr());
        lastRow.getCell(6).setCellValue(row.getCpc());
        lastRow.getCell(8).setCellValue(row.getConvRate());
        lastRow.getCell(9).setCellValue(row.getCpa());
        lastRow.getCell(11).setCellValue(row.getRoas());
        lastRow.getCell(12).setCellValue(row.getAvgPos());

        XSSFCellStyle lastRowStyle= lastRow.getCell(1).getCellStyle();
/*
		lastRow.createCell(2).setCellStyle(lastRowStyle);
		lastRow.createCell(2).setCellFormula("SUM(C28:C"+(lastRowNumber)+")"); //impressions
		lastRow.createCell(3).setCellFormula("SUM(D28:D"+(lastRowNumber)+")"); //clicks
		lastRow.createCell(5).setCellFormula("SUM(F28:F"+(lastRowNumber)+")"); //cost
		lastRow.createCell(7).setCellFormula("SUM(H28:H"+(lastRowNumber)+")");  //conversions
		lastRow.createCell(10).setCellFormula("SUM(K28:K"+(lastRowNumber)+")"); //revenue*/


        plotChart(sheet);

    }

}
