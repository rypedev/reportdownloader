package workbook;

import models.*;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import repositories.WeeklyKeywordsData;

import java.util.ArrayList;

/**
 * Created by Phani.Teja on 15-Feb-17.
 */
public class WeeklyKeywordsService extends PopulateDataService{
    public static XSSFSheet sheet;
    ArrayList<CommonFields> list;

    public void prepareSheet(XSSFSheet sheet) {
        this.sheet=sheet;
        WeeklyKeywordsData campaignData=new WeeklyKeywordsData();
        list=campaignData.getCurrentWeeksData();

        int currentRow=populateTable(4);
        list.clear();
        list=campaignData.getLastWeeksData();
        populateTable(currentRow+4);


    }

    public int populateTable(int rowNumber){
        int currentWeeksDataRow=rowNumber;
        XSSFRow currentRow;

        sheet.shiftRows(rowNumber,sheet.getLastRowNum(),list.size());
        for(CommonFields dataRow:list){
            WeeklyKeywords row=(WeeklyKeywords) dataRow;
            currentRow=sheet.createRow(currentWeeksDataRow);

            currentRow.createCell(1).setCellValue("Google");
            currentRow.createCell(2).setCellValue(row.getCampaignName());
            currentRow.createCell(3).setCellValue(row.getAdGroupName());
            currentRow.createCell(4).setCellValue(row.getKeyword());
            currentRow.createCell(5).setCellValue(row.getImpressions());
            currentRow.createCell(6).setCellValue(row.getClicks());
            currentRow.createCell(7).setCellValue(row.getCtr());
            currentRow.createCell(8).setCellValue(row.getCost());
            currentRow.createCell(9).setCellValue(row.getCpc());
            currentRow.createCell(10).setCellValue(row.getConversions());
            currentRow.createCell(11).setCellValue(row.getConvRate());
            currentRow.createCell(12).setCellValue(row.getCpa());
            currentRow.createCell(13).setCellValue(row.getRevenue());
            currentRow.createCell(14).setCellValue(row.getRoas());
            currentRow.createCell(15).setCellValue(row.getAvgPos());
            currentWeeksDataRow++;
        }

        XSSFRow lastRow= sheet.getRow(currentWeeksDataRow);

        CommonFields row=calculateFinalValues(list);
        //row.print();
        if(lastRow==null){
            lastRow=sheet.createRow(currentWeeksDataRow);
            System.out.println("Creating new last row");
            lastRow.createCell(5).setCellValue(row.getImpressions());
            lastRow.createCell(6).setCellValue(row.getClicks());
            lastRow.createCell(8).setCellValue(row.getCost());
            lastRow.createCell(10).setCellValue(row.getConversions());
            lastRow.createCell(13).setCellValue(row.getRevenue());

            lastRow.createCell(7).setCellValue(row.getCtr());
            lastRow.createCell(9).setCellValue(row.getCpc());
            lastRow.createCell(11).setCellValue(row.getConvRate());
            lastRow.createCell(12).setCellValue(row.getCpa());
            lastRow.createCell(14).setCellValue(row.getRoas());
            lastRow.createCell(15).setCellValue(row.getAvgPos());
        } else {
            lastRow.getCell(5).setCellValue(row.getImpressions());
            lastRow.getCell(6).setCellValue(row.getClicks());
            lastRow.getCell(8).setCellValue(row.getCost());
            lastRow.getCell(10).setCellValue(row.getConversions());
            lastRow.getCell(13).setCellValue(row.getRevenue());

            lastRow.getCell(7).setCellValue(row.getCtr());
            lastRow.getCell(9).setCellValue(row.getCpc());
            lastRow.getCell(11).setCellValue(row.getConvRate());
            lastRow.getCell(12).setCellValue(row.getCpa());
            lastRow.getCell(14).setCellValue(row.getRoas());
            lastRow.getCell(15).setCellValue(row.getAvgPos());
        }

        currentWeeksDataRow++;
        return currentWeeksDataRow;

    }

}
