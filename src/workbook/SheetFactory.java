package workbook;

/**
 * Created by Phani.Teja on 17-Feb-17.
 */
public class SheetFactory {
    public static PopulateDataService service;

    /*
        sheet 0 : Title Page
        sheet 1 : Weekly Trends
        sheet 2 : Weekly Summary - Campaigns
        sheet 3 : Weekly Summary - AdGroups
        sheet 4 : Weekly Summary - Keywords
        sheet 5 : Monthly Summary*/
    public static PopulateDataService getService(String sheetName){
        switch (sheetName){
            case "Title Page":
                service= new TitlePageService();
                break;
            case "Weekly Trends":
                service= new WeeklyTrendsService();
                break;
            case "Weekly Summary - Campaigns":
                service=new WeeklyCampaignService();
                break;
            case "Weekly Summary - AdGroups":
                service=new WeeklyAdGroupService();
                break;
            case "Weekly Summary - Keywords":
                service=new WeeklyKeywordsService();
                break;
            case "Monthly Summary":
                service=new MonthlyTrendsService();
                break;
            default:
                service=null;
        }
        return service;
    }
}
