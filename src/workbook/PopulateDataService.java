package workbook;

import models.CommonFields;
import org.apache.poi.ss.usermodel.charts.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFChart;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.charts.XSSFChartLegend;
import org.apache.poi.xssf.usermodel.charts.XSSFLineChartData;

import java.util.ArrayList;

/**
 * Created by Phani.Teja on 17-Feb-17.
 */
public  abstract class PopulateDataService {

    public abstract void  prepareSheet(XSSFSheet sheet);

    public CommonFields calculateFinalValues(ArrayList<CommonFields> list){
        CommonFields finalValues=new CommonFields();
        for(CommonFields item:list){
            finalValues.setImpressions(finalValues.getImpressions()+item.getImpressions());
            finalValues.setClicks(finalValues.getClicks()+item.getClicks());
            finalValues.setCost(finalValues.getCost()+item.getCost());
            finalValues.setConversions(finalValues.getConversions()+item.getConversions());
            finalValues.setRevenue(finalValues.getRevenue()+item.getRevenue());
            finalValues.setAvgPos(finalValues.getAvgPos()+item.getAvgPos());
        }
        finalValues.setCtr((float)finalValues.getClicks()/finalValues.getImpressions());
        finalValues.setCpc((float)finalValues.getCost()/finalValues.getClicks());
        finalValues.setConvRate((float)finalValues.getConversions()/finalValues.getClicks());
        finalValues.setCpa((float)finalValues.getCost()/finalValues.getConversions());
        finalValues.setRoas((float)(finalValues.getRevenue()-finalValues.getCost())/finalValues.getRevenue());
        finalValues.setAvgPos(finalValues.getAvgPos()/list.size());

        return  finalValues;
    }

    public void plotChart(XSSFSheet sheet){
        XSSFSheet my_worksheet=sheet;
        XSSFDrawing xlsx_drawing = my_worksheet.createDrawingPatriarch();


        XSSFClientAnchor anchor = xlsx_drawing.createAnchor(0, 0, 0, 0, 1, 5, 13, 25);
        XSSFChart my_line_chart = xlsx_drawing.createChart(anchor);

        //XSSFChart scatterChart=xlsx_drawing.createChart(anchor);
        XSSFChartLegend legend = my_line_chart.getOrCreateLegend();
        legend.setPosition(LegendPosition.BOTTOM);
        XSSFLineChartData data= my_line_chart.getChartDataFactory().createLineChartData();
        //XSSFScatterChartData data1 = scatterChart.getChartDataFactory().createScatterChartData();
        ChartAxis bottomAxis = my_line_chart.getChartAxisFactory().createCategoryAxis(AxisPosition.BOTTOM);
        ValueAxis leftAxis = my_line_chart.getChartAxisFactory().createValueAxis(AxisPosition.LEFT);
        //ValueAxis rightAxis = my_line_chart.getChartAxisFactory().createValueAxis(AxisPosition.LEFT);
        leftAxis.setCrosses(AxisCrosses.AUTO_ZERO);
        //rightAxis.setCrosses(AxisCrosses.AUTO_ZERO);
        int lastRowNumber=sheet.getLastRowNum()-1;
        ChartDataSource<Number> xs = DataSources.fromNumericCellRange(my_worksheet, new CellRangeAddress(27,lastRowNumber,1,1));
        ChartDataSource<Number> ys1 = DataSources.fromNumericCellRange(my_worksheet, new CellRangeAddress(27,lastRowNumber,7,7));
        ChartDataSource<Number> ys2 = DataSources.fromNumericCellRange(my_worksheet, new CellRangeAddress(27,lastRowNumber,9,9));
        LineChartSeries series1=data.addSeries(xs,ys1);
        series1.setTitle("Conversions");
        LineChartSeries series2=data.addSeries(xs, ys2);
        series2.setTitle("CPA");
        my_line_chart.plot(data, new ChartAxis[] { bottomAxis, leftAxis });
        //my_line_chart.plot(data1, new ChartAxis[] { bottomAxis, leftAxis });
    }
}
