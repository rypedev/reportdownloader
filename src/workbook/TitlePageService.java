package workbook;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import repositories.TitlePageData;
import models.TitlePage;

/**
 * Created by Phani.Teja on 16-Feb-17.
 */
public class TitlePageService extends PopulateDataService{

    public void prepareSheet(XSSFSheet sheet){

        TitlePageData data=new TitlePageData();
        TitlePage accountData=data.getDataFromDB();
        if(sheet.getRow(4)==null) {
            sheet.createRow(4);
        }
        sheet.createRow(4).createCell(1).setCellValue(accountData.getAddress());
        sheet.getRow(21).getCell(4).setCellValue(accountData.getCompanyName());
        sheet.getRow(22).getCell(4).setCellValue(accountData.getWebUrl());
        sheet.getRow(23).getCell(4).setCellValue(accountData.getManagerName());

        return;
    }
}
