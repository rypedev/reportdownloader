/*
package workbook;

import models.WeeklyTrends;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DataPopulation {
	
	static Connection connection;
	
	

	
	public ArrayList<WeeklyTrends> getWeeklyChartData(){
		getConnection();
		Statement stmt;
		try {
			stmt = connection.createStatement();
			String query="CALL CalculateWeeklyData('2017-01-24' , '2017-02-06' , '9066992631')";
			ResultSet rs=stmt.executeQuery(query);
			System.out.println(rs.getMetaData());
			return populateResultSet(rs);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}


	public ArrayList<WeeklyTrends> populateResultSet(ResultSet rs) {
		ArrayList<WeeklyTrends> finalData=new ArrayList<WeeklyTrends>();

		try {
			while (rs.next()) {
				String end_of_week = rs.getString("end_of_week");
				int impressions = rs.getInt("impressions");
				int clicks = rs.getInt("clicks");
				float ctr = rs.getFloat("CTR");
				int cost = rs.getInt("cost");
				float cpc = rs.getFloat("CPC");
				int conversions = rs.getInt("conversions");
				float convRate = rs.getFloat("convRate");
				float cpa = rs.getFloat("CPA");
				int revenue = rs.getInt("revenue");
				float roas = rs.getFloat("ROAS");
				float avgPos = rs.getFloat("avgPos");

				WeeklyTrends row = new WeeklyTrends(end_of_week, impressions, clicks, ctr, cost, cpc, conversions, convRate, cpa, revenue, roas, avgPos);

				finalData.add(row);
			}
			return finalData;
		} catch(SQLException e){
			System.out.println(e.getStackTrace());
		}

		return null;
	}

	public static void main(String args[]){
		DataPopulation data=new DataPopulation();
		data.getWeeklyChartData();
	}

}
*/
