package workbook;


import java.io.File;
import java.io.FileOutputStream;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class PrepareExcel {
	public static String accountId;
	public static String endDate;
	public static String fileName;


	public String getAccountId() {
		return accountId;
	}

	public PrepareExcel() {
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}



	public PrepareExcel(String accountId, String endDate, String fileName) {
		this.accountId = accountId;
		this.endDate = endDate;
		this.fileName = fileName;
	}

	/*
            sheet 0 : Title Page
            sheet 1 : Weekly Trends
            sheet 2 : Weekly Summary - Campaigns
            sheet 3 : Weekly Summary - AdGroups
            sheet 4 : Weekly Summary - Keywords
            sheet 5 : Monthly Summary*/
	public void getWorkbook() {
		try {			 
			XSSFWorkbook wb = new XSSFWorkbook(new File("newTemplate.xlsx"));

			int noOfSheets = wb.getNumberOfSheets();

			System.out.println(noOfSheets);
			for (int i = 0; i < noOfSheets; i++) {
				String sheetName=wb.getSheetName(i);
				System.out.println("sheet " + i + " : " +sheetName );
					XSSFSheet sheet = wb.getSheet(sheetName);
					PopulateDataService service;
					service = SheetFactory.getService(sheetName);
					service.prepareSheet(sheet);
			}
/*

			XSSFSheet titleSheet= wb.getSheet("Title Page");
			TitlePageService titlePageService=new TitlePageService();
			titlePageService.prepareSheet(titleSheet);

			XSSFSheet weeklySheet=wb.getSheet("Weekly Trends");

			WeeklyTrendsService service=new WeeklyTrendsService();
			service.prepareSheet(weeklySheet);

			XSSFSheet monthlySheet=wb.getSheet("Monthly Summary");
			MonthlyTrendsService monthlyService=new MonthlyTrendsService();
			monthlyService.prepareSheet(monthlySheet);

			XSSFSheet weeklyCampaignSheet=wb.getSheet("Weekly Summary - Campaigns");
			WeeklyCampaignService campaignService=new WeeklyCampaignService();
			campaignService.prepareSheet(weeklyCampaignSheet);

			XSSFSheet weeklyAdgroupSheet=wb.getSheet("Weekly Summary - AdGroups");
			WeeklyAdGroupService adGroupService=new WeeklyAdGroupService();
			adGroupService.prepareSheet(weeklyAdgroupSheet);

			XSSFSheet weeklyKeyWordsSheet=wb.getSheet("Weekly Summary - Keywords");
			WeeklyKeywordsService keywordsService=new WeeklyKeywordsService();
			keywordsService.prepareSheet(weeklyKeyWordsSheet);
*/
			wb.setActiveSheet(1);
			FileOutputStream stream = new FileOutputStream(fileName);
			wb.write(stream);
			stream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
