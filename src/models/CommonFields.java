package models;

/**
 * Created by Phani.Teja on 14-Feb-17.
 */
public class CommonFields {

    public int getImpressions() {
        return impressions;
    }

    public void setImpressions(int impressions) {
        this.impressions = impressions;
    }

    public int getClicks() {
        return clicks;
    }

    public CommonFields(){

    }

    public CommonFields(int impressions, int clicks, float ctr, int cost, float cpc, int conversions, float convRate, float cpa, int revenue, float roas, float avgPos) {
        this.impressions = impressions;
        this.clicks = clicks;
        this.ctr = ctr;
        this.cost = cost;
        this.cpc = cpc;
        this.conversions = conversions;
        this.convRate = convRate;
        this.cpa = cpa;
        this.revenue = revenue;
        this.roas = roas;
        this.avgPos = avgPos;
    }

    public void setClicks(int clicks) {
        this.clicks = clicks;
    }

    public float getCtr() {
        return ctr;
    }

    public void setCtr(float ctr) {
        this.ctr = ctr;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public float getCpc() {
        return cpc;
    }

    public void setCpc(float cpc) {
        this.cpc = cpc;
    }

    public int getConversions() {
        return conversions;
    }

    public void setConversions(int conversions) {
        this.conversions = conversions;
    }

    public float getConvRate() {
        return convRate;
    }

    public void setConvRate(float convRate) {
        this.convRate = convRate;
    }

    public float getCpa() {
        return cpa;
    }

    public void setCpa(float cpa) {
        this.cpa = cpa;
    }

    public int getRevenue() {
        return revenue;
    }

    public void setRevenue(int revenue) {
        this.revenue = revenue;
    }

    public float getRoas() {
        return roas;
    }

    public void setRoas(float roas) {
        this.roas = roas;
    }

    public float getAvgPos() {
        return avgPos;
    }

    public void setAvgPos(float avgPos) {
        this.avgPos = avgPos;
    }

    int impressions;
    int clicks;
    float ctr;
    int cost;
    float cpc;
    int conversions;
    float convRate;
    float cpa;
    int revenue;
    float roas;
    float avgPos;

    public void print(){
        System.out.println("impressions:"+impressions);
        System.out.println("clicks:"+clicks);
        System.out.println("ctr:"+ctr);
        System.out.println("cost:"+cost);
        System.out.println("cpc:"+cpc);
        System.out.println("conversions:"+conversions);
        System.out.println("cpa:"+cpa);
        System.out.println("revenue:"+revenue);
        System.out.println("roas:"+roas);
        System.out.println("avg pos:"+avgPos);


    }
}
