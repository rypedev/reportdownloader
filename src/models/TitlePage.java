package models;

/**
 * Created by Phani.Teja on 16-Feb-17.
 */
public class TitlePage {

    String accountName;
    String address;

    public TitlePage(String accountName, String address, String companyName, String webUrl, String managerName) {
        this.accountName = accountName;
        this.address = address;
        this.companyName = companyName;
        this.webUrl = webUrl;
        this.managerName = managerName;
    }

    public TitlePage() {
    }

    String companyName;

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    String webUrl;
    String managerName;

}
