package models;

/**
 * Created by Phani.Teja on 14-Feb-17.
 */
public class MonthlyTrends  extends CommonFields{

    public MonthlyTrends(){

    }
    private String month;

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public MonthlyTrends(String month,int impressions, int clicks, float ctr, int cost, float cpc, int conversions, float convRate, float cpa, int revenue, float roas, float avgPos) {
        super(impressions, clicks, ctr, cost, cpc, conversions, convRate, cpa, revenue, roas, avgPos);
        this.month = month;
    }
}
