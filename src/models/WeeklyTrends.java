package models;

/**
 * Created by Phani.Teja on 14-Feb-17.
 */
public class WeeklyTrends extends CommonFields{

    public WeeklyTrends(String endOfWeek,int impressions, int clicks, float ctr, int cost, float cpc, int conversions, float convRate, float cpa, int revenue, float roas, float avgPos) {
        super(impressions, clicks, ctr, cost, cpc, conversions, convRate, cpa, revenue, roas, avgPos);
        this.endOfWeek = endOfWeek;
    }

    public WeeklyTrends(){

    }

    String endOfWeek;

    public String getEndOfWeek() {
        return endOfWeek;
    }

    public void setEndOfWeek(String endOfWeek) {
        this.endOfWeek = endOfWeek;
    }

}
