package models;

/**
 * Created by Phani.Teja on 15-Feb-17.
 */
public class WeeklyKeywords extends CommonFields {
    public String campaignName;
    public String searchEngine;

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String keyword;

    public String getAdGroupName() {
        return adGroupName;
    }

    public void setAdGroupName(String adGroupName) {
        this.adGroupName = adGroupName;
    }

    public String adGroupName;

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    public String getSearchEngine() {
        return searchEngine;
    }

    public void setSearchEngine(String searchEngine) {
        this.searchEngine = searchEngine;
    }

    public WeeklyKeywords(String campaignName ,String adGroupName,String keyword,int impressions, int clicks, float ctr, int cost, float cpc, int conversions, float convRate, float cpa, int revenue, float roas, float avgPos) {
        super(impressions, clicks, ctr, cost, cpc, conversions, convRate, cpa, revenue, roas, avgPos);
        this.campaignName = campaignName;
        this.searchEngine = "Google";
        this.adGroupName = adGroupName;
        this.keyword=keyword;
    }

    public WeeklyKeywords() {
    }
}
